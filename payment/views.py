from flask import Blueprint, render_template

payment_bp = Blueprint('payment_bp', __name__, url_prefix='/payment', template_folder='templates')


@payment_bp.route('/')
def index():
    return render_template('payments/index.html')
