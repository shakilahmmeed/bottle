from flask import Blueprint, render_template

blog_blueprint = Blueprint('blog_blueprint', __name__, url_prefix='/blog', template_folder='templates')


@blog_blueprint.route('/')
def index():
    return render_template('blog/blog.html')
