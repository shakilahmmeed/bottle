from flask import Flask
from general.views import index_blueprint
from blog.views import blog_blueprint
from payment.views import payment_bp

app = Flask(__name__)
app.register_blueprint(index_blueprint)
app.register_blueprint(blog_blueprint)
app.register_blueprint(payment_bp)

